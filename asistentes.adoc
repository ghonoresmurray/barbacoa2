== Lista de asistentes
// Ordenados por orden alfabético
// Formato: Apellidos, Nombre

* Almarcha Conejero, Joaquin
* Bazan Ticse, Kevin
* Blanco Lucas, Abel
* Carrero Núñez, Jesús
* Diaz Rios, Álvaro
* Fenet López, Adrián
* Guerra García, Diego Andrés
* Honores Murray,Gustavo
* Luque Giráldez, José Rafael
* Marchena Curado, Francisco Javier
* Molina Garcia, Santiago
* Postigo Martínez, Jesús Juan
* Romero Pastor, Iván
* Rodríguez Otero, Antonio
* Ruiz Jurado, Pablo
* Sánchez García, María Jesús
* Valdivieso Casado, José Antonio
* Zapico Gallo, Miguel

=== Organización del transporte

// Si tienes vehículo, pon el número de plazas. Si no tienes, añádete a
// alguno de los vehículos existentes.

==== Citroën Xsara (5 plazas)

* Fenet López, Adrián
* Guerra García, Diego Andrés
* Luque Giráldez, José Rafael

==== Volkswagen Golf (5 plazas)

* Romero Pastor, Iván
* Ruiz Jurado, Pablo

==== Kia Ceed (5 plazas)

* Molina Garcia, Santiago
* Sánchez García, María Jesús
* Valdivieso Casado, José Antonio
* Zapico Gallo, Miguel

==== Audi A3(5 plazas)

* Carrero Núñez, Jesús
* Postigo Martínez, Jesús Juan
* Rodríguez Otero, Antonio

=== Opel Zafira(7 plazas)
* Diaz Rios, Álvaro
* Blanco Lucas, Abel
=======

=== Tesla Model 3(4 plazas)
* Honores Murray,Gustavo
